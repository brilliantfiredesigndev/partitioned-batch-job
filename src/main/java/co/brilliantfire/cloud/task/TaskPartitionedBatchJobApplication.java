package co.brilliantfire.cloud.task;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;

@SpringBootApplication
@EnableBatchProcessing
@EnableTask
public class TaskPartitionedBatchJobApplication {

//	@Bean
//	public CommandLineRunner commandLineRunner() {
//		return strings ->
//				System.out.println("Executed at :" + 
//				      new SimpleDateFormat().format(new Date()));
//	}
	
	public static void main(String[] args) {
		SpringApplication.run(TaskPartitionedBatchJobApplication.class, args);
	}
}
